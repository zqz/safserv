#include "mailtask.h"

#include <iostream> // TODO: remove

MailTask::MailTask( const Mailer &mailer, RequestPtr req )
    : _req( req )
    , _mailer( mailer )
{
}

void MailTask::work()
{
    Email email( _req->_addressFrom,
                 _req->_nameFrom,
                 _req->_addressTo,
                 _req->_subject,
                 _req->_body );

    _mailer.send( email );
}

