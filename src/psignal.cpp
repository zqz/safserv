#include "psignal.h"

#include <unistd.h>
#include <fcntl.h>
#include <string.h>

Psignal::Psignal()
{
    if ( pipe( _fd ) == -1 )
    {
        throw std::runtime_error( std::string( "pipe:") + strerror( errno ) );
    }

    int flags = fcntl( _fd[0], F_GETFL );
    if ( fcntl( _fd[0], F_SETFL, flags | O_NONBLOCK ) == -1 )
    {
        closefds();
        throw std::runtime_error( std::string( "pip nb mode:") + strerror( errno ) );
    }
}

Psignal::~Psignal()
{
    closefds();
}

void Psignal::closefds()
{
    close( _fd[0] );
    close( _fd[1] );
}

int Psignal::getFd() const noexcept
{
    return _fd[0];
}

bool Psignal::set()
{
    std::lock_guard<std::mutex> l( _mutex );

    char c[] = "1";
    if ( write( _fd[1], c, 1 ) <= 0 )
    {
        return false;
    }

    return true;
}

bool Psignal::reset()
{
    std::lock_guard<std::mutex> l( _mutex );

    char c[1] = {0};
    return ( read( _fd[0], c, 1 ) > 0 );
}

