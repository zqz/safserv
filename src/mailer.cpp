#include "mailer.h"

#include <array>
#include <random>
#include <sstream>
#include <cstring>
#include <ctime>

#include <curl/curl.h>

///////////////////////////////////////////////////////////////////////////////
// utilities

std::string formatCurrentDate()
{
    std::array<char, 64> buf;

    time_t rawtime;
    tm *timeinfo;

    time( &rawtime );
    timeinfo = localtime( &rawtime );

    strftime( buf.data(), buf.size(), "%a, %d %b %Y %H:%M:%S %z", timeinfo );
        
    return buf.data();
}

std::string generateMessageId()
{
    std::stringstream ss;
    time_t t = std::time(nullptr);
    ss << t << ".";

    std::random_device rd;
    std::mt19937 mt( rd() );
    std::uniform_int_distribution<int> distr(9000, 1000000);
    ss << distr( mt ) << ".";
    ss << t + distr( mt );

    return ss.str();
}

std::string getDomainFromAddress( const std::string &address )
{
    size_t pos = address.find_first_of( "@" );
    if ( pos != std::string::npos )
    {
        return address.substr( pos + 1 );
    }

    return "";
}

class Curl
{
    CURL *_curl;

public:
    Curl()
    {
        _curl = curl_easy_init();
        if ( !_curl )
            throw std::runtime_error("cannot init curl");
    }

    ~Curl()
    {
        curl_easy_cleanup(_curl);
    }

    inline operator CURL*() const
    {
        return _curl;
    }
};

///////////////////////////////////////////////////////////////////////////////
Email::Email( const std::string &addressFrom,
              const std::string &nameFrom,
              const std::string &addressTo,
              const std::string &subject,
              const std::string &body )
    : _addressFrom( addressFrom )
    , _addressTo( addressTo )
{
    createLines( addressFrom, nameFrom, addressTo, subject, body );
}

void Email::createLines( const std::string &addressFrom,
                  const std::string &nameFrom,
                  const std::string &addressTo,
                  const std::string &subject,
                  const std::string &body)
{
    _lines.clear();
    _lines.emplace_back( "Date: " + formatCurrentDate() + "\r\n" );
    _lines.emplace_back( "To: <" + addressTo + ">\r\n");
    _lines.emplace_back( "From: <" + addressFrom + "> (" + nameFrom + ")\r\n" );
    _lines.emplace_back( "Message-ID: <" + generateMessageId() + "@" );
    _lines.emplace_back( getDomainFromAddress( addressFrom ) + ">\r\n" );
    _lines.emplace_back( "Subject: " + subject + "\r\n");
    _lines.emplace_back( "\r\n" );

    std::stringstream ss( body );
    std::string sb;
    while( getline( ss, sb ) )
    {
        _lines.emplace_back( sb );
    }
}

bool Email::getLine( int index, std::string *line ) const
{
    if ( index < static_cast< int >( _lines.size() ) )
    {
        *line = _lines[ index ];
        return true;
    }

    line->clear();
    return false;
}

std::string Email::getAddressFrom() const
{
    return _addressFrom;
}

std::string Email::getAddressTo() const
{
    return _addressTo;
}

///////////////////////////////////////////////////////////////////////////////
Mailer::Mailer( const std::string &username,
                const std::string &password,
                const std::string &smtp)
    : _username( username )
    , _password( password )
    , _smtp( smtp )
{
}

size_t Mailer::payload_source(void *ptr, size_t size, size_t nmemb, void *userp)
{
    UploadContext *ctx = static_cast< UploadContext* >( userp );
 
    if ((size == 0) || (nmemb == 0) || ((size*nmemb) < 1)) {
        return 0;
    }

    std::string s;
    if ( ctx->msg->getLine( ctx->lines_read++, &s ) )
    {
        size_t len = s.size();
        memcpy( ptr, s.c_str(), len );
        return len;
    }
 
    return 0;
}

void Mailer::send( const Email &msg ) const
{
    Curl curl;

    CURLcode res = CURLE_OK;
    curl_slist *recipients = NULL; // TODO: use autoobject
        
    UploadContext upload_ctx;
    upload_ctx.lines_read = 0;
    upload_ctx.msg = &msg;

    curl_easy_setopt(curl, CURLOPT_USERNAME, _username.c_str());
    curl_easy_setopt(curl, CURLOPT_PASSWORD, _password.c_str());
    curl_easy_setopt(curl, CURLOPT_URL, _smtp.c_str());

    curl_easy_setopt(curl, CURLOPT_USE_SSL, (long)CURLUSESSL_ALL);

    curl_easy_setopt(curl, CURLOPT_MAIL_FROM, msg.getAddressFrom().c_str());

    recipients = curl_slist_append(recipients, msg.getAddressTo().c_str());
    curl_easy_setopt(curl, CURLOPT_MAIL_RCPT, recipients);

    curl_easy_setopt(curl, CURLOPT_READFUNCTION, payload_source);
    curl_easy_setopt(curl, CURLOPT_READDATA, &upload_ctx);
    curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);

//    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); // uncomment to print debug info
    res = curl_easy_perform(curl);

    curl_slist_free_all(recipients);

    if ( res != CURLE_OK )
    {
        throw std::runtime_error(
            "mail sending failed:" +
            std::string( curl_easy_strerror(res) ) );
    }
}

