#include "config.h"

#include <iostream> // TODO:
#include <algorithm>
#include <cctype>
#include <fstream>
#include <tuple>
#include <map>

void trim( std::string &s, const char *ws )
{
    s.erase( 0, s.find_first_not_of( ws ) );
    s.erase( s.find_last_not_of( ws ) + 1 );
}

void to_lower( std::string &s )
{
    std::transform( s.begin(), s.end(), s.begin(), ::tolower );
}

std::tuple< std::string, std::string > splitParam( const std::string &param, const char *ws )
{
    size_t splitPos = param.find_first_of( ws );
    std::string name = param.substr( 0, splitPos );
    to_lower( name );

    std::string val;

    if ( splitPos != std::string::npos )
    {
        val = param.substr( splitPos );
        trim( val, ws );
    }

    return std::make_tuple( std::move( name ), std::move( val ) );
}

void Config::fillParameter( const std::string &name, const std::string &val )
{
    if ( name == "smtp_username" )
        _smtpLogin = val;
    else if ( name == "smtp_password" )
        _smtpPassword = val;
    else if ( name == "smtp_server" )
        _smtpServer = val;
    else if ( name == "pid" )
        _pidFile = val;
    else if ( name == "port" )
        _listenPort = atoi( val.c_str() );
}

void Config::read( const std::string &filename )
{
    std::ifstream fs( filename );
    if ( !fs )
        throw std::runtime_error("cannot read config file");

    const char* ws = " \t";

    std::string s;
    while( getline( fs, s ) )
    {
        trim( s, ws );
        if ( s.empty() || s[ 0 ] == '#' )
            continue;

        auto params = splitParam( s, ws );
        fillParameter( std::get<0>( params ), std::get<1>( params ) );
    }
}

