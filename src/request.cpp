#include "request.h"

#include <iostream>

Request::Request()
    : _state( StateAddressFrom )
{
};

void Request::Print()
{
    std::cout << "From: " << _addressFrom << std::endl;
    std::cout << "FromName: " << _nameFrom << std::endl;
    std::cout << "To: " << _addressTo << std::endl;
    std::cout << "Subject: " << _subject << std::endl;
    std::cout << "Body: " << _body << std::endl;
}

class StateAddressFromProc : public StateProcessor
{
public:
    virtual ParseState run(char input, Request *req)
    {
        if ( input == '\n' )
        {
            req->_state = StateNameFrom;
        }
        else if ( std::isprint( input ) )
        {
            req->_addressFrom.push_back( input );
        }
        else if ( input == 0 )
        {
            req->_state = StateError;
        }

        return req->_state;
    }
};

class StateNameFromProc : public StateProcessor
{
public:
    virtual ParseState run(char input, Request *req)
    {
        if ( input == '\n' )
        {
            req->_state = StateAddressTo;
        }
        else if ( std::isprint( input ) )
        {
            req->_nameFrom.push_back( input );
        }
        else if ( input == 0 )
        {
            req->_state = StateError;
        }

        return req->_state;
    }
};

class StateAddressToProc : public StateProcessor
{
public:
    virtual ParseState run(char input, Request *req)
    {
        if ( input == '\n' )
        {
            req->_state = StateSubject;
        }
        else if ( std::isprint( input ) )
        {
            req->_addressTo.push_back( input );
        }
        else if ( input == 0 )
        {
            req->_state = StateError;
        }

        return req->_state;
    }
};

class StateSubjectProc : public StateProcessor
{
public:
    virtual ParseState run(char input, Request *req)
    {
        if ( input == '\n' )
        {
            req->_state = StateBody;
        }
        else if ( std::isprint( input ) )
        {
            req->_subject.push_back( input );
        }
        else if ( input == 0 )
        {
            req->_state = StateError;
        }

        return req->_state;
    }
};

class StateAddressBodyProc : public StateProcessor
{
public:
    virtual ParseState run(char input, Request *req)
    {
        if ( input == 0 )
        {
            req->_state = StateEnd;
        }
        else if ( std::isprint( input ) || std::isspace( input ) )
        {
            req->_body.push_back( input );
        }

        return req->_state;
    }
};

RequestParser::RequestParser()
{
    _stateProc.resize( ParseStateCount );

    _stateProc[ StateAddressFrom ] = std::make_unique< StateAddressFromProc >();
    _stateProc[ StateNameFrom ] = std::make_unique< StateNameFromProc >();
    _stateProc[ StateAddressTo ] = std::make_unique< StateAddressToProc >();
    _stateProc[ StateSubject ] = std::make_unique< StateSubjectProc >();
    _stateProc[ StateBody ] = std::make_unique< StateAddressBodyProc >();
}

RequestState RequestParser::parse(const char *data, int len, Request *req) const
{
    for ( int i = 0; i < len; ++i )
    {
        const StateProcessorPtr &proc = _stateProc[ req->_state ];
        if ( !proc.get() )
        {
            return ReqError;
        }

        ParseState res = proc->run( data[ i ], req );
        if ( res == StateError )
        {
            return ReqError;
        }

        if ( res == StateEnd )
        {
            return ReqComplete;
        }
    }

    return ReqIncomplete;
}

