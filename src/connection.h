#pragma once

#include <future>
#include <list>

#include "request.h"

struct ConnectionEvents
{
    virtual void OnRequestReceived( RequestPtr req ) = 0;
};

class Connection
{
    int _sock;
    Psignal &_stopSig;
    std::future< void > _future;
    RequestParser &_reqParser;
    RequestPtr _req;
    ConnectionEvents *_events;

    void workingProc() noexcept;
    Connection( const Connection& ) = delete; // noncopyable
    Connection& operator=( const Connection& ) = delete;

public:
    Connection( 
        int sock, 
        Psignal &stopSig, 
        RequestParser &reqParser,
        ConnectionEvents *events );
    virtual ~Connection();
    std::future< void >& result();

protected:
    virtual bool OnReceive( const char* data, int len );
};

class Connections
{
    Psignal &_stopSig;
    std::list< Connection > _conns;
    std::thread _watcher;
    RequestParser _reqParser;
    ConnectionEvents *_events;
    static const int _watchPeriod;

    void watchingProc() noexcept;

public:
    Connections( Psignal &stopSig, ConnectionEvents *events );
    ~Connections();
    void add( int sock );
};

