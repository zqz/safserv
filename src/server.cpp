#include <iostream>
#include <exception>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <string.h>
#include <syslog.h>

#include "server.h"
#include "mailtask.h"

Server::Server(
    const std::string &smtpUsername,
    const std::string &smtpPassword,
    const std::string &smtpServer)
    : _conns( _stopSig, this )
    , _running( false )
    , _taskPool( 2 )
    , _mailer( smtpUsername, smtpPassword, smtpServer )
{
}

Server::~Server()
{
    Stop();
}

void Server::threadProc( int listener )
{
    _running = true;
        
    try
    {
        int stopFd = _stopSig.getFd();            
        int maxFd = std::max( listener, stopFd );
        fd_set readset;

        while ( true )
        {
            FD_ZERO(&readset);
            FD_SET(listener, &readset);
            FD_SET(stopFd, &readset);

            int res = select(maxFd + 1, &readset, NULL, NULL, NULL);
            if ( res == -1 )
            {
                throw std::runtime_error( std::string( "listen select:" ) + strerror( errno ) );
            }
            else if ( res == 0 )
            {
                syslog( LOG_WARNING, "listen select unxepected result" );
                continue;
            }

            if ( FD_ISSET(listener, &readset) )
            {
                sockaddr_in client;
                int cl_size = sizeof(client);

                int sock = accept(listener, (sockaddr*)&client, (socklen_t*)&cl_size);
                if ( sock == -1 )
                {
                    throw std::runtime_error( std::string( "accept:" ) + strerror( errno ) );
                }

                char clbuf[ INET_ADDRSTRLEN ] = {0};
                inet_ntop(AF_INET, &client.sin_addr, clbuf, INET_ADDRSTRLEN);

                this->OnAccept(sock, clbuf);
            }

            if ( FD_ISSET(stopFd, &readset) )
            {
                break;
            }
        }
    }
    catch (std::runtime_error &e)
    {
        syslog( LOG_ERR, "%s", e.what() );
    }

    _running = false;
    close(listener);
}

void Server::Start( short port )
{
    if ( _running )
        return;

    sockaddr_in addr;
    int listener = socket(AF_INET, SOCK_STREAM, 0);
    if ( listener < 0 )
    {
        throw std::runtime_error( std::string( "open listener:" ) + strerror( errno ) );
    }

    fcntl(listener, F_SETFL, O_NONBLOCK);

    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;

    if ( bind(listener, (sockaddr*)&addr, sizeof(addr)) < 0 )
    {
        close(listener);
        throw std::runtime_error( std::string( "bind listener:" ) + strerror( errno ) );
    }

    listen(listener, 2);

    _listenThread = std::thread( &Server::threadProc, this, listener );
}

void Server::Stop()
{
    _stopSig.set();

    if ( _listenThread.joinable() )
    {
        _listenThread.join();
    }
}

void Server::OnAccept( int sock, const std::string &addrFrom )
{
    // TODO: add log
    _conns.add( sock );
}

void Server::OnRequestReceived( RequestPtr req )
{
    _taskPool.addTask( std::make_shared<MailTask>( _mailer, req ) );
}

