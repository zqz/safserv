#pragma once

#include <queue>
#include <memory>
#include <list>
#include <mutex>
#include <thread>
#include <atomic>
#include <condition_variable>

struct Task
{
    Task();
    virtual void work() = 0;
};

typedef std::shared_ptr< Task > TaskPtr;

class TaskPool
{
    std::list<std::thread> _threads;
    std::queue< TaskPtr > _tasks;
    std::atomic<bool> _stop;
    std::mutex _m;
    std::condition_variable _cv;

public:
    TaskPool(int threadNum);
    ~TaskPool();
    void addTask( TaskPtr task );
};

