#pragma once

#include <string>

class PidFile
{
    int _fd;
    bool _locked;
    std::string _filename;

public:
    PidFile( const char *filename );
    ~PidFile();

    PidFile( const PidFile& ) = delete; // noncopyable
    PidFile& operator=( const PidFile& ) = delete;

    bool tryLock();
    bool writePid();
    void unlock();
};

