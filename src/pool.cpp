#include "safserv.h"
#include "pool.h"

#include <syslog.h>
#include <exception>

using namespace std;

Task::Task()
{
}

TaskPool::TaskPool(int threadNum)
    : _stop( false )
{
    for (int i = 0; i < threadNum; ++i )
    {
        _threads.emplace_back(
            [this, id=i]()
            {
                while( true )
                {
                    unique_lock<mutex> lock( this->_m );
                    this->_cv.wait( 
                        lock,
                        [this] { return this->_stop || !this->_tasks.empty(); } );

                    if ( this->_stop )
                    {
                        return;
                    }

                    TaskPtr task = this->_tasks.front();
                    this->_tasks.pop();
                    lock.unlock();

                    try
                    {
                        task->work();
                    }
                    catch ( std::exception &e )
                    {
                        syslog( LOG_ERR, "%s", e.what() );
                    }
                }
            } );
    }
}

TaskPool::~TaskPool()
{
    _stop = true;
    _cv.notify_all();

    for ( thread& t: _threads )
    {
        t.join();
    }
}

void TaskPool::addTask( TaskPtr task )
{
    unique_lock<mutex> l( _m );
    _tasks.emplace( task );
        
    _cv.notify_one();
}

