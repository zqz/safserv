#pragma once

#include "request.h"
#include "pool.h"
#include "mailer.h"

class MailTask : public Task
{
    RequestPtr _req;
    const Mailer &_mailer;

public:
    MailTask( const Mailer &mailer, RequestPtr req );
    virtual void work() override;
};

