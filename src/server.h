#pragma once

#include <thread>
#include "psignal.h"
#include "connection.h"
#include "pool.h"
#include "mailer.h"

class Server : public ConnectionEvents
{
    std::thread _listenThread;
    Psignal _stopSig;
    Connections _conns;
    std::atomic<bool> _running;
    TaskPool _taskPool;
    Mailer _mailer;

    void threadProc( int listener );
    void Stop(); // mustn't be stopped manually

 public:
    Server( 
        const std::string &smtpUsername,
        const std::string &smtpPassword,
        const std::string &smtpServer );
    virtual ~Server();
    void Start( short port );
    virtual void OnAccept( int sock, const std::string &addrFrom );
    virtual void OnRequestReceived( RequestPtr req ) override;
};

