#pragma once

#include <string>

class Config final
{
    void fillParameter( const std::string &name, const std::string &val );

public:
    std::string _smtpLogin;
    std::string _smtpPassword;
    std::string _smtpServer;
    std::string _pidFile;
    short _listenPort;

    void read( const std::string &filename );
};

