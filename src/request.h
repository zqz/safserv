#pragma once

#include <string>
#include <vector>
#include <memory>

enum RequestState
{
    ReqIncomplete = 0,
    ReqComplete = 1,
    ReqError = 2
};

enum ParseState
{
    StateError = 0,
    StateEnd = 1,
    StateAddressFrom = 2,
    StateNameFrom = 3,
    StateAddressTo = 4,
    StateSubject = 5,
    StateBody = 6,

    ParseStateCount = 7
};

struct Request
{
    std::string _addressFrom;
    std::string _nameFrom;
    std::string _addressTo;
    std::string _subject;
    std::string _body;

    ParseState _state;

    Request();
    void Print(); // TODO: remove
};

typedef std::shared_ptr< Request > RequestPtr;

struct StateProcessor
{
    virtual ParseState run(char input, Request *req) = 0;
};

typedef std::unique_ptr< StateProcessor > StateProcessorPtr;
typedef std::vector< StateProcessorPtr > StateProcessors;

class RequestParser final
{
    StateProcessors _stateProc;

public:
    RequestParser();
    RequestState parse(const char *data, int len, Request *req) const;
};

