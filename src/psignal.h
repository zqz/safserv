#pragma once

#include <mutex>

class Psignal final
{
    int _fd[2];
    std::mutex _mutex;

    void closefds();

public:
    Psignal();
    Psignal( const Psignal& ) = delete; // noncopyable
    Psignal& operator=( const Psignal& ) = delete;
    ~Psignal();

    int getFd() const noexcept;
    bool set();
    bool reset();
};

