#include "safserv.h"
#include "pool.h"
#include "server.h"
#include "pidfile.h"
#include "config.h"

#include <memory>

#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <signal.h>
#include <string.h>
#include <syslog.h>
#include <sys/stat.h>

using namespace std;

class Syslog // TODO: move to other file
{
public:
    Syslog( const char *name )
    {
        openlog( name, LOG_PID | LOG_NDELAY, LOG_USER );
    }

    ~Syslog()
    {
        closelog();
    }
};

enum ProgramReturnCode
{
    Successful = 0,
    AlreadyRunning = 1,
    DaemonFailed = 2,
    DaemonFailed2 = 3,
    CriticalFailure = 4
};

void daemonProc( const Config &conf )
{
    syslog( LOG_INFO, "safserv started on port( %d )", conf._listenPort );

    sigset_t sigset;
    int signo;
    sigemptyset( &sigset );

    sigaddset( &sigset, SIGTERM );

    sigprocmask( SIG_BLOCK, &sigset, NULL );

    try
    {
        Server srv( conf._smtpLogin, conf._smtpPassword, conf._smtpServer );
        srv.Start( conf._listenPort );

        sigwait( &sigset, &signo );
    }
    catch ( exception &e )
    {
        syslog( LOG_ERR, "%s", e.what() );
    }

    syslog( LOG_INFO, "safserv stopped" );
}

int main(int argc, char *argv[])
{
    std::string confPath( "safserv.conf" );
    if ( argc > 1 )
    {
        confPath = argv[1];
    }

    Config conf;
    unique_ptr< PidFile > pidFile;

    try
    {
        conf.read( confPath );

        pidFile = make_unique< PidFile >( conf._pidFile.c_str() );
        if ( !pidFile->tryLock() )
        {
            cout << "another instance is already running" << endl;
            return AlreadyRunning;
        }
    }
    catch ( exception &e )
    {
        cout << e.what() << endl;
        return CriticalFailure;
    }

    int pid = fork();
    if ( pid < 0 ) // error
    {
        cout << strerror( errno ) << endl;
        return DaemonFailed;
    }
    else if ( pid == 0 )
    {
        umask( 0 );
        setsid();
        chdir( "/" );

        close( STDIN_FILENO );
        close( STDOUT_FILENO );
        close( STDERR_FILENO );
        
        Syslog log("safserv");

        pid = fork();
        if ( pid < 0 )
        {
            syslog( LOG_ERR, "start daemon failed: %s", strerror( errno ) );
            return DaemonFailed2;
        }
        else if ( pid == 0 )
        {
            pidFile->writePid();
            daemonProc( conf );
            pidFile->unlock();
        }
    }

    return Successful;
}

