#pragma once

#include <string>
#include <vector>

class Email
{
    std::string _addressFrom;
    std::string _addressTo;
    std::vector<std::string> _lines;

    void createLines( const std::string &addressFrom,
                      const std::string &nameFrom,
                      const std::string &addressTo,
                      const std::string &subject,
                      const std::string &body);

public:
    Email( const std::string &addressFrom,
           const std::string &nameFrom,
           const std::string &addressTo,
           const std::string &subject,
           const std::string &body );

    bool getLine( int index, std::string *line ) const;
    std::string getAddressFrom() const;
    std::string getAddressTo() const;
};

class Mailer
{
    std::string _username;
    std::string _password;
    std::string _smtp;

    struct UploadContext
    {
        int lines_read;
        const Email *msg;
    };

    static size_t payload_source(void *ptr, size_t size, size_t nmemb, void *userp);

public:
    Mailer( const std::string &username,
            const std::string &password,
            const std::string &smtp);

    void send( const Email &msg ) const;
};

