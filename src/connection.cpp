#include "psignal.h"
#include "connection.h"

#include <unistd.h>
#include <string.h>
#include <syslog.h>
#include <iostream>
#include <future>
#include <chrono>
#include <thread>

Connection::Connection( 
    int sock, 
    Psignal &stopSig, 
    RequestParser &reqParser,
    ConnectionEvents *events )
    : _sock( sock )
    , _stopSig( stopSig )
    , _reqParser( reqParser )
    , _req( std::make_shared< Request >() )
    , _events( events )
{
    _future = async( std::launch::async, &Connection::workingProc, this );
}

Connection::~Connection()
{
     close( _sock ); // will interrupt the working proc (if it's not stopped yet )
    _future.wait();
}

std::future< void >& Connection::result()
{
    return _future;
}

void Connection::workingProc() noexcept
{
    try
    {
        int stopFd = _stopSig.getFd();
        fd_set rfds;
        int maxFd = std::max( _sock, stopFd );
        char readbuf[0xff];

        while(1)
        {
            FD_ZERO( &rfds );
            FD_SET( _sock, &rfds );
            FD_SET( stopFd, &rfds );

            int res = select( maxFd + 1, &rfds, NULL, NULL, NULL );
            if ( res == -1 )
            {
                throw std::runtime_error( std::string( "conn select:" ) + strerror( errno ) );
            }

            if ( FD_ISSET( stopFd, &rfds ) )
            {
                break;
            }
            
            if ( FD_ISSET( _sock, &rfds ) )
            {
                int rc = read( _sock, readbuf, sizeof(readbuf) );
                if ( rc == -1 )
                {
                    throw std::runtime_error( std::string( "conn read: " ) + strerror( errno ) );
                }

                if ( rc == 0 ) // disconnected
                {
                    break;
                }

                if ( !OnReceive( readbuf, rc ) )
                    break;
            }
        }
    }
    catch( std::runtime_error& e )
    {
        syslog( LOG_ERR, "%s", e.what() );
    }

    close( _sock );
}

bool Connection::OnReceive( const char* data, int len )
{
    RequestState res = _reqParser.parse( data, len, _req.get() );
    if ( res == ReqComplete )
    {
        _events->OnRequestReceived( _req );
        _req->Print();
        // TODO: send response
        return false;
    }
    else if ( res == ReqError )
    {
        syslog( LOG_ERR, "request has invalid format" );
        return false;
    }

    return true;
}

//////////////////////////////////////////////////
const int Connections::_watchPeriod = 60;

Connections::Connections( Psignal &stopSig, ConnectionEvents *events )
    : _stopSig( stopSig )
    , _events( events )
{
    _watcher = std::thread( &Connections::watchingProc, this );
}

Connections::~Connections()
{
    if ( _watcher.joinable() )
    {
        _watcher.join();
    }
}

void Connections::add( int sock )
{
    _conns.emplace_back( sock, _stopSig, _reqParser, _events );
}

void Connections::watchingProc() noexcept
{
    try
    {
        int stopFd =_stopSig.getFd();
        fd_set rfds;
        struct timeval tv;

        while (1)
        {
            FD_ZERO( &rfds );
            FD_SET( stopFd, &rfds );

            tv.tv_sec = _watchPeriod;
            tv.tv_usec = 0;

            int res = select( stopFd + 1, &rfds, NULL, NULL, &tv );
            if ( res == -1 )
            {
                throw std::runtime_error( std::string( "watching select:") + strerror( errno ) );
            }

            if ( res == 0 )
            {
                using namespace std::chrono_literals;

                _conns.remove_if([]( Connection &c ) // remove closed connections
                {
                    return ( c.result().wait_for(0s) == std::future_status::ready );
                });
                continue;
            }

            if ( FD_ISSET( stopFd, &rfds ) )
            {
                break;
            }
        }
    }
    catch( std::runtime_error& e )
    {
        syslog( LOG_ERR, "%s", e.what() );
    }
}

