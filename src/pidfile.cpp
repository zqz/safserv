#include <string.h>
#include <sys/file.h>
#include <unistd.h>

#include <array>
#include <exception>

#include "pidfile.h"

PidFile::PidFile( const char *filename )
    : _locked( false )
    , _filename( filename )
{
    _fd = open( filename, O_WRONLY | O_CREAT, 0666 );
    if ( _fd == -1 )
    {
        throw std::runtime_error( std::string( "open pid:" ) + strerror( errno ) );
    }
}

PidFile::~PidFile()
{
    if ( _fd >= 0 )
        close( _fd );
}

bool PidFile::tryLock()
{
    if ( flock( _fd, LOCK_EX | LOCK_NB ) == -1 )
    {
        if ( errno == EWOULDBLOCK )
        {
            return false;
        }

        throw std::runtime_error( std::string( "lock pid:" ) + strerror( errno ) );
    }

    _locked = true;        

    return true;
}

bool PidFile::writePid()
{
    bool result = false;

    if ( _locked )
    {
        ftruncate( _fd, 0 );

        std::array< char, 32 > buf;
        snprintf( buf.data(), buf.size(), "%ld", static_cast< long >( getpid() ) );
        int len = strlen( buf.data() );

        if ( write( _fd, buf.data(), len ) == len )
        {
            result = true;
        }
    }

    return result;
}

void PidFile::unlock()
{
    if ( _locked )
    {
        flock( _fd, LOCK_UN | LOCK_NB );
        close( _fd );
        _fd = -1;

        unlink( _filename.c_str() );
    }
}
