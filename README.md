**SafServ**
Very light service that performs tasks (only sends emails in this version) asynchronously.

Uses CURL library (https://curl.haxx.se/) to send emails.  
#####sudo apt-get install libcurl4-gnutls-dev  
built checked with Ubuntu Linux using:

-cmake 2.8.12.2  
-clang 3.4  
-gcc 5.4.1 (minimum version)  
-curl 7.35.0  

####project is in development

**Packet format:**

sender@email.address\n  
Mail Sender Name\n  
recipient@email.address\n  
subject\n  
email body\0
